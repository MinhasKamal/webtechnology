var current_img_id=1;

function show(id){
	current_img_id=id;
  	change();
}

function next(){
	current_img_id++;
	if(current_img_id>6){
		current_img_id=1;
	}
    change();
}

 function previous(){
	current_img_id--;
	if(current_img_id<1){
		current_img_id=6;
	}
 	change();
}

function change(){
	document.getElementById("main_view").src = document.getElementById("short_view").children[current_img_id].src;
}