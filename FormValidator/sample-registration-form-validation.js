function formValidation(){
	
	var userid_length = document.registration.userid.value.length;
	if(userid_length<5) {
		if(userid_length==0) {
			alert("You must submit your User ID.");
		}else{
			alert("User ID must be between 5-12 characters. \nYours is less than 5 characters.");
		}
		document.registration.userid.focus();
		return false;
    }
			
	var passid_length = document.registration.passid.value.length;
	if(passid_length<7) {
		if(passid_length==0) {
			alert("You must type your password.");
		}else{
			alert("Password must be between 7-12 characters. \nYours is less than 7 characters.");
		}
		document.registration.userid.focus();
		return false;
    }
	
	var alphabets = /^[A-Z a-z]+$/;  
	if(!document.registration.username.value.match(alphabets)){	  
		alert('User name must have alphabet characters only.');  
		document.registration.username.focus();  
		return false;
	}

    var alphanumerics = /^[0-9 a-z A-Z]+$/;  
    if(!document.registration.address.value.match(alphanumerics))  {  
		alert('User address must have alphanumeric characters only.');  
		document.registration.address.focus();  
		return false;  
    }  
	
	if(document.registration.country.value == "Default") {
        alert("You must choose a country.");
        document.registration.country.focus();
        return false;
    }
	
	if(document.registration.zip.value == ""){ 
		alert("You must enter your ZIP code.");
        document.registration.zip.focus();
        return false;
    }else if(isNaN(document.registration.zip.value)){
		alert("Your ZIP code must be a number.");
        document.registration.zip.focus();
        return false;
	}
	
	var mail_format = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/; 
	if(!document.registration.email.value.match(mail_format)) {
        alert("This email address is invalid.");
        document.registration.email.focus();
        return false;
    }
	
	if(document.registration.sex[0].checked == false && document.registration.sex[1].checked == false){
        alert("You must select your sex.");
		document.registration.sex.focus();
        return false;
    }
	
	if(document.registration.en.checked == false && document.registration.nonen.checked == false){
        alert("You must select a language.");
		document.registration.en.focus();
        return false;
    }
	
	return true;
}